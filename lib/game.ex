defmodule Game do
    use GenServer
  
    def init({secret, correct, wrong, turns_left}) do
      {:ok, {secret, correct, wrong, turns_left}}
    end
  
    def handle_call(:get_feedback, _from, game) do
      {:reply, Hangman.format_feedback(game), game}
    end
  
    def handle_cast({:submit_guess, guess}, game) do
      {:noreply, game}
    end
end